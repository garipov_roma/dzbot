#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import config
import telebot
import math
import urllib3
import urllib.request
import threading
import os
import random

from bs4 import BeautifulSoup

def get_html(url):
	response = urllib.request.urlopen(url)
	return response.read()	

def parse(html):
	soup = BeautifulSoup(html, "lxml")
	wall = soup.find('div', class_ = "wall_posts")
	posts = list(wall.find_all('div', class_ = 'pi_text'))
	return posts
	
def main():
	site = str('h' + 't' + 't' + 'p' + 's' + '://' + 'vk' + '.' + 'com' + '/10b_131')
	wall = list(parse(get_html(site)))#return wall[1]
	return str(wall[1])

def convert(s):
	n = int(len(s))
	res = ''
	i = 0
	while(i < n):
		if (s[i] == '>'):
			i = i + 1
		if (i + 4 < n):
			if (s[i] == '<' and s[i + 1] == 'b' and s[i + 2] == 'r' and s[i + 3] == '/' and s[i + 4] == '>'):
				res = res + '\n'
				i = i + 5
		
		if (s[i] == '<'):
			while(not(s[i] == '>') and i < n):
				i = i + 1
		if (i >= n - 1):
			break
		if (s[i] == '>'):
			continue
		res = res + s[i]
		i = i + 1
	return res

#------------------------------PARSER-----------------------------------------------------------------
#------------------------------PARSER-----------------------------------------------------------------
#------------------------------PARSER-----------------------------------------------------------------
#------------------------------PARSER-----------------------------------------------------------------
#------------------------------PARSER-----------------------------------------------------------------
#------------------------------PARSER-----------------------------------------------------------------
#------------------------------PARSER-----------------------------------------------------------------
#------------------------------PARSER-----------------------------------------------------------------


bot = telebot.TeleBot(config.token)


@bot.message_handler(commands = ['dz'])
def dz(message):
	msg = message
	#print(message)
	this_chat = msg.chat.id
	print(msg.chat.first_name)
	try:
		res = convert(main())
		#print(res)
		bot.send_message(this_chat, res)
	except Exception:
		bot.send_message(this_chat, 'Error')



def remove_file(s):
	path = os.path.join(os.path.abspath(os.path.dirname(__file__)), s)
	os.remove(path)
	
subs = set()

fin = open('output.txt', 'r')

mystr = fin.read()
print(mystr)
mystr = mystr.split('\n')

m = int(mystr[0])

for i in range(1, m + 1):
	a = int(mystr[i])
	subs.add(a)

fin.close()

@bot.message_handler(commands = ['sub'])
def kek(message):	
	if (message.chat.id in subs):
		subs.remove(message.chat.id)
		remove_file('output.txt')
		fout = open('output.txt', 'w')
		fout.write(str(len(subs)) + '\n')
		for i in subs:
			fout.write(str(i) + '\n')
		fout.close()
		bot.send_message(message.chat.id, 'Вы отписались от рассылки')
	else:	
		remove_file('output.txt')
		fout = open('output.txt', 'w')
		subs.add(message.chat.id)
		fout.write(str(len(subs)) + '\n')
		for i in subs:
			fout.write(str(i) + '\n')
		fout.close()
		bot.send_message(message.chat.id, 'Вы подписались на рассылку')
	print(subs)

def send_all():
	res = convert(main())
	for i in subs:
		bot.send_message(i, res)

last_time = ""

def update_site():
	s = convert(main())
	global last_time
	if (last_time != "" and last_time != s):
		send_all()
	last_time = s
	threading.Timer(10, update_site).start()

if __name__ == '__main__':
	update_site()
	bot.polling(none_stop=True)
