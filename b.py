#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import config
import telebot
import math
import urllib3
import urllib.request
import random

from bs4 import BeautifulSoup

def get_html(url):
	response = urllib.request.urlopen(url)
	return response.read()	

def get_confirmity(s):
	a = list([])
	bal = int(0)

def parse(html):
	soup = BeautifulSoup(html, "lxml")
	wall = soup.find('div', class_ = "wall_posts")
	posts = list(wall.find_all('div', class_ = 'pi_text'))
	return posts
	
def main():
	site = str('h' + 't' + 't' + 'p' + 's' + '://' + 'vk' + '.' + 'com' + '/10b_131')
	wall = list(parse(get_html(site)))#return wall[1]
	return str(wall[1])

def convert(s):
	n = int(len(s))
	res = ''
	i = 0
	while(i < n):
		if (s[i] == '>'):
			i = i + 1
		if (i + 4 < n):
			if (s[i] == '<' and s[i + 1] == 'b' and s[i + 2] == 'r' and s[i + 3] == '/' and s[i + 4] == '>'):
				res = res + '\n'
				i = i + 5
		
		if (s[i] == '<'):
			while(not(s[i] == '>') and i < n):
				i = i + 1
		if (i >= n - 1):
			break
		if (s[i] == '>'):
			continue
		res = res + s[i]
		i = i + 1
	return res

print(main())

bot = telebot.TeleBot(config.token)

@bot.message_handler(commands = ['dz'])
def dz(message):
	msg = message
	this_chat = msg.chat.id
	print(msg.chat.first_name)
	try:
		res = convert(main())
		print(res)
		bot.send_message(this_chat, res)
	except Exception:
		bot.send_message(this_chat, 'Error')

#if __name__ == '__main__':
#     bot.polling(none_stop=True)
